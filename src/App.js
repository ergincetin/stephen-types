import './App.css';
import React from 'react';
import Example from "./Example.tsx";
import Colors from './Colors';
import Objects from "./Annotations/objects";
import Arr from "./Annotations/arrays.tsx";
function App() {
  return (
    <div className="App">
     <Example />

     <Colors/>

     <Arr/>
    </div>
  );
}

export default App;
