const oldHonda = {
    name: "civic",
    year: 2008,
    corrupted: false,
    summary( ): string {
        return `Name: ${this.name}  - Year: ${this.year} - Corruption: ${this.corrupted}`
    }
     
}

interface Vehicle {
    summary(): string 
}


const printVehicle = (vehicle: Vehicle):void => {
console.log(vehicle.summary())

}

printVehicle(oldHonda)

export {}