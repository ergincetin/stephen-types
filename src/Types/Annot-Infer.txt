Variables -> Functions -> Objects

Type annotation: (we tell TS the type)
the code we add to tell Typescript what type of value a variable will refer to.

Type inference: (TS guesses the type)
TS tries to figure out what type of value a var. refers to
