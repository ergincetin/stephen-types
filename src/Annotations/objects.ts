const profile = {
    name: "adam",
    lastname: "copeland",
    physics: {
      height: 196,  
      weight: 260,
    },
    titles: 11,
    nick: "Edge",    
    age: 47,

    updateTitles(titles: number):void {
        this.titles=titles
    }
};

const {titles} : {titles: number} = profile 


export default profile; //line for not to get error