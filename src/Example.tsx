class Person {

    constructor(name: string, id: number){
        this.name=name;
        this.id=id
    }

    name:string;
    id:number;
}

const me = new Person( "beyza", 7897);


export default function UsePerson () {
    return (
        <div>
            {me.id} - {me.name}
        </div>
    )
}