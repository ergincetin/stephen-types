import { stringify } from "querystring";

class Vehicle {

    //color: string="red"; or construct this field in constructor in order to 
    //be able to use it by argument passing.
color:string;
    constructor(color:string){
        this.color=color;
    }
    private drive():void {
        console.log( "vehicle started");
    }
    

    protected runDrive(): void {
        this.drive();
    }
}

class Car extends Vehicle {
    
    stop():void {
        console.log("stopped!");
    }

    Testdrive(): void {
        console.log("driving...") //overriding a function
    }
}

let mycol:string="blue"
const veh1= new Vehicle(mycol)


//const veh1 = new Vehicle();
//veh1.drive(); //cannot be exec. as its private and can only be used within VEHİCLE class
//veh1.runDrive(); //cannot be exec. as its private and can only be used within VEHİCLE's  methods and its subclasses

// const car1= new Car();
// car1.Testdrive();

// const veh2 = new Vehicle();



export default Vehicle;