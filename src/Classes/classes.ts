class Vehicle {
    drive():void {
        console.log( "vehicle started");
    }
}

class Car extends Vehicle {
    
    stop():void {
        console.log("stopped!");
    }

    drive(): void {
        console.log("driving...") //overriding a function
    }
}

const veh1 = new Vehicle();
veh1.drive();
const car1= new Car();
car1.stop();
car1.drive();



export {}